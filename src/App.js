
import './App.css';
import SnapshotHome from './components/Snapshot';
import ImageContainer from './components/images'


function App() {
  return (
    <div>
      <SnapshotHome />
      <ImageContainer />
    </div>
  );
}

export default App;
