import { Component } from 'react'
import { BsSearch } from 'react-icons/bs'
import Loader from 'react-loader-spinner'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

import './index.css'


let userInput = "Mountain"
let url = `https://pixabay.com/api/?key=29524649-38a788ef67a971e9618f01c16&q=${userInput}&image_type=photo&pretty=true`


class ImageContainer extends Component {
    state = {
        imagesArray: [],
        userInput: "Mountain",
        isLoading: true,
    }
    DisplaySearchImages = (event) => {
        this.setState({ imagesArray: [], userInput: event.target.value, isLoading: true })
        setTimeout(() => {
            this.getBlogItemData(url)
        }, 800)
    }

    clickingSearch = () => {
        const { userInput } = this.state
        this.setState({ imagesArray: [], userInput: userInput, isLoading: true })
        setTimeout(() => {
            this.getBlogItemData(url)
        }, 800)
    }

    DisplayImages = (event) => {
        let keyvalue = event.target.id

        this.setState({ imagesArray: [], userInput: keyvalue, isLoading: true })
        setTimeout(() => {
            this.getBlogItemData(url)
        }, 800)
    }
    componentDidMount() {
        this.getBlogItemData(url)
    }

    getBlogItemData = async (url) => {
        const { userInput } = this.state
        url = `https://pixabay.com/api/?key=29524649-38a788ef67a971e9618f01c16&q=${userInput}&image_type=photo&pretty=true`
        const response = await fetch(url)
        const data = await response.json()
        this.setState({
            imagesArray: data.hits,
            isLoading: false
        })
    }

    renderingButton = () => {
        const { imagesArray, isLoading } = this.state
        if (isLoading === true) {
            return (<Loader type="TailSpin" color="#051c33" height={50} width={50} className="LoaderEl" key="loader" />)
        } else if (imagesArray.length !== 0) {
            let result = (imagesArray.map((object) => {
                let imgurl = object.webformatURL
                let id = object.id
                return (<li className='list-item-image-container' key={id}>
                    <img src={imgurl} alt="profile-pic" className='image-details' />
                </li>)
            }))
            return result
        } else {
            return (<li className='Error-container' key="Error">
                <h1 className='Error-heading'>No Images Found</h1>
                <p className='Error-suggetion'>Please try a different search term</p>
            </li>)
        }
    }

    render() {
        const { userInput } = this.state

        return (<div className='image-container'>
            <div className='search-container'>
                <input type='text' placeholder='Search...' className='input-element' value={userInput} onChange={this.DisplaySearchImages} />
                <div className='search-icon-container' onClick={this.clickingSearch}>
                    <BsSearch className='search-icon' />
                </div>
            </div>
            <div className='btns-container'>
                <div className='btn-sub-container-1'>
                    <button className='btn' onClick={this.DisplayImages} id="Mountain">
                        Mountain
                    </button>
                    <button className='btn' onClick={this.DisplayImages} id="Beaches">Beaches</button>
                </div>
                <div className='btn-sub-container-2'>
                    <button className='btn' onClick={this.DisplayImages} id="Birds">Birds</button>
                    <button className='btn' onClick={this.DisplayImages} id="Food">Food</button>
                </div>
            </div>
            <h2 className='pictures-heading'>{userInput} Pictures</h2>
            <ul className='unorderedList'>
                {this.renderingButton()}
            </ul>
        </div>)

    }
}

export default ImageContainer